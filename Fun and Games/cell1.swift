//
//  cell1.swift
//  Fun and Games
//
//  Created by Emma Freeman on 4/20/15.
//  Copyright (c) 2015 freemanebensonc. All rights reserved.
//

import UIKit

class cell1: UITableViewCell {

    @IBOutlet var label1: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
