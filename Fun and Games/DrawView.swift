//
//  DrawView.swift
//  Fun and Games
//
//  Created by Emma Freeman on 4/20/15.
//  Copyright (c) 2015 freemanebensonc. All rights reserved.
//

import UIKit

class DrawView: UIView {

    var lines: [Line] = []

    var lastPoint: CGPoint!
    
    required init(coder aDecoder: (NSCoder!)) {
        super.init(coder: aDecoder)
    }

    override func touchesBegan(touches: NSSet, withEvent event: (UIEvent!)) {
        lastPoint = touches.anyObject()!.locationInView(self)
    }

    
    override func touchesMoved(touches: NSSet!, withEvent event: (UIEvent!)){
        var newPoint: CGPoint! = touches.anyObject()!.locationInView(self)
        lines.append(Line(start: lastPoint, end: newPoint))
        lastPoint = newPoint
        
        self.setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect){
        var context = UIGraphicsGetCurrentContext()
        CGContextBeginPath(context)
        for line in lines{
            CGContextMoveToPoint(context, line.start.x, line.start.y)
            CGContextAddLineToPoint(context, line.end.x, line.end.y)
        }
        CGContextSetLineCap(context, kCGLineCapRound)
        CGContextSetRGBStrokeColor(context, 0, 0, 0, 1)
        CGContextSetLineWidth(context, 5)
        CGContextStrokePath(context)
    }



}
