//
//  ViewController.swift
//  Fun and Games
//
//  Created by Emma Freeman on 4/20/15.
//  Copyright (c) 2015 freemanebensonc. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var activities:[String] = ["Draw!", "Tic Tac Toe", "Family Feud"]
    
    @IBOutlet var drawView : AnyObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activities.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell1return = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath) as! cell1
        
        cell1return.label1.text = activities[indexPath.row]
        
        return cell1return
        
    }
    
    @IBAction func clearTapped() {
        var theDrawView: DrawView = drawView as! DrawView
        theDrawView.lines = []
        theDrawView.setNeedsDisplay()
        
    }
    
}

