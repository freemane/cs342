//
//  Line.swift
//  Fun and Games
//
//  Created by Emma Freeman on 4/20/15.
//  Copyright (c) 2015 freemanebensonc. All rights reserved.
//

import UIKit

class Line{
    
    var start: CGPoint
    var end: CGPoint
    
    init(start varStart: CGPoint, end varEnd: CGPoint){
        start = varStart
        end = varEnd
    }
    
    
}






